<?php
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
  wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
  //    wp_enqueue_style( 'custom-style', get_stylesheet_directory_uri() . '/custom-style.css', array('parent-style')); // 子テーマのcss


  ///////////////////////////////////////
  // 自前でプロフィール画像の設定
  ///////////////////////////////////////
  //プロフィール画面で設定したプロフィール画像
  if ( !function_exists( 'get_the_author_upladed_avatar_url_demo' ) ):
  function get_the_author_upladed_avatar_url_demo($user_id){
    if (!$user_id) {
      $user_id = get_the_posts_author_id();
    }
    return esc_html(get_the_author_meta('upladed_avatar', $user_id));
  }
  endif;

  //ユーザー情報追加
  add_action('show_user_profile', 'add_avatar_to_user_profile_demo');
  add_action('edit_user_profile', 'add_avatar_to_user_profile_demo');
  if ( !function_exists( 'add_avatar_to_user_profile_demo' ) ):
  function add_avatar_to_user_profile_demo($user) {
  ?>
    <h3>プロフィール画像</h3>
    <table class="form-table">
      <tr>
        <th>
          <label for="avatar">プロフィール画像URL</label>
        </th>
        <td>
        	<input type="text" name="upladed_avatar" size="70" value="<?php echo get_the_author_upladed_avatar_url_demo($user->ID); ?>" placeholder="画像URLを入力してください">
         <p class="description">Gravatarよりこちらのプロフィール画像が優先されます。240×240pxの正方形の画像がお勧めです。</p>
        </td>
      </tr>
    </table>
  <?php
  }
  endif;

  //入力した値を保存する
  add_action('personal_options_update', 'update_avatar_to_user_profile_demo');
  if ( !function_exists( 'update_avatar_to_user_profile_demo' ) ):
  function update_avatar_to_user_profile_demo($user_id) {
    if ( current_user_can('edit_user',$user_id) ){
      update_user_meta($user_id, 'upladed_avatar', $_POST['upladed_avatar']);
    }
  }
  endif;

  //プロフィール画像を変更する
  add_filter( 'get_avatar' , 'get_uploaded_user_profile_avatar_demo' , 1 , 5 );
  if ( !function_exists( 'get_uploaded_user_profile_avatar_demo' ) ):

  function get_uploaded_user_profile_avatar_demo( $avatar, $id_or_email, $size, $default, $alt ) {
    if ( is_numeric( $id_or_email ) )
      $user_id = (int) $id_or_email;
    elseif ( is_string( $id_or_email ) && ( $user = get_user_by( 'email', $id_or_email ) ) )
      $user_id = $user->ID;
    elseif ( is_object( $id_or_email ) && ! empty( $id_or_email->user_id ) )
      $user_id = (int) $id_or_email->user_id;

    if ( empty( $user_id ) )
      return $avatar;

    if (get_the_author_upladed_avatar_url_demo($user_id)) {
      $alt = !empty($alt) ? $alt : get_the_author_meta( 'display_name', $user_id );;
      $author_class = is_author( $user_id ) ? ' current-author' : '' ;
      $avatar = "<img alt='" . esc_attr( $alt ) . "' src='" . esc_url( get_the_author_upladed_avatar_url_demo($user_id) ) . "' class='avatar avatar-{$size}{$author_class} photo' height='{$size}' width='{$size}' />";
    }

    return $avatar;
  }
  endif;


  /* ===========================================
  ショートコード用カスタム投稿タイプ
  =========================================== */
  add_action('init', 'my_custom_post_type');
  function my_custom_post_type() {
    $labels = array(
      'name' => 'ショートコード',
      'singular_name' => 'ショートコード',
      'menu_name' => 'ショートコード',
      'name_admin_bar' => 'ショートコード',
      'add_new_item' => '新規投稿を追加',
      'add_new' => '新規追加',
      'new_item' => '新規投稿',
      'view_item' => '投稿を表示',
      'not_found' => '投稿が見つかりませんでした。',
      'not_found_in_trash' => 'ゴミ箱内に投稿が見つかりませんでした。',
      'search_items' => '投稿を検索'
    );

    $args = array(
      'labels' => $labels,
      'public' => false,
      'query_var' => false,
      'capability_type' => 'post',
      'show_ui' => true,
      'hierarchical' => false,
      'has_archive' => false,
      'rewrite' => array( 'slug' => false, 'with_front' => false ),
      'supports' => array( 'title', 'editor', 'excerpt', 'author', 'revisions' ),
      'menu_position' => null,
      'menu_icon' => 'dashicons-editor-code'
    );

    register_post_type('my_embed_codes', $args);
  }

  /* ===========================================
  ショートコード用カスタムフィールド
  =========================================== */
  add_action('add_meta_boxes', 'my_add_meta_box');
  function my_add_meta_box() {
    add_meta_box(
      'my-embed-settings',
      'ショートコード用カスタムフィールド',
      'my_meta_box_callback',
      'my_embed_codes'
    );
  }

  function my_meta_box_callback($post) {
    wp_nonce_field('my-embed-nonce', '_wpnonce_embed');

    $meta = get_post_meta($post->ID, 'hogehoge', true);
    $html = '<input type="text" name="hogehoge" class="regular-text" value="'.esc_attr($meta).'">';

    echo $html;
  }

  add_action('save_post', 'my_save_meta_box');
  function my_save_meta_box($post_id) {
    if (isset($_POST['hogehoge'])
    && isset($_POST['_wpnonce_embed'])
    && wp_verify_nonce($_POST['_wpnonce_embed'], 'my-embed-nonce')) {
      update_post_meta($post_id, 'hogehoge', $_POST['hogehoge']);
    }
  }

  /* ===========================================
  記事一覧カスタマイズ
  =========================================== */
  //項目
  add_filter('manage_my_embed_codes_posts_columns', 'my_posts_columns');
  function my_posts_columns($columns) {
    //unset($columns['cb']); //チェックボックス
    //unset($columns['title']); //タイトル
    //unset($columns['author']); //著者
    unset($columns['categories']); //カテゴリー
    unset($columns['tags']); //タグ
    unset($columns['comments']); //コメント
    //unset($columns['date']); //日付

    $columns['shortcode'] = __('ショートコード');

    array_multisort(array(0, 1, 3, 4, 2), $columns); //並び替え

    return $columns;
  }

  //内容
  add_action('manage_my_embed_codes_posts_custom_column', 'my_posts_custom_column', 10, 2);
  function my_posts_custom_column($column, $post_id) {
    if ($column === 'shortcode') {
      $code = '[my-embed-code id="'.$post_id.'"]';
      echo '<input type="text" onfocus="this.select();" value="'.esc_attr($code).'" class="large-text code" readonly>';
    }
  }

  //編集リンク
  add_filter('post_row_actions', 'my_post_row_actions', 10, 2);
  function my_post_row_actions($actions, $post){
    if ($post->post_type == 'my_embed_codes') {
      //unset( $actions['edit'] ); //編集
      unset($actions['inline hide-if-no-js']); //クイック編集
      //unset( $actions['trash'] ); //ゴミ箱
    }

    return $actions;
  }

  /* ===========================================
  投稿画面カスタマイズ
  =========================================== */
  add_action('admin_head-post.php', 'my_admin_post_style');
  add_action('admin_head-post-new.php', 'my_admin_post_style');
  function my_admin_post_style(){
    global $typenow;

    if ($typenow == 'my_embed_codes') {
      //公開オプションを非表示
      echo '<style type="text/css">#misc-publishing-actions, #minor-publishing-actions { display:none; }</style>'."\n";
    }
  }

  /* ===========================================
  ショートコード追加
  =========================================== */
  add_shortcode('my-embed-code', '__return_false'); //後記のフィルターのためここでは実行しない
  function my_shortcode($atts) {
    $code = shortcode_atts( array(
      'id' => null
    ), $atts );

    if ($code['id'] && get_post($code['id'])) {
      $type = get_post_type($code['id']); //カスタム投稿タイプ名

      if ($type == 'my_embed_codes') {
        $title = get_the_title($code['id']); //投稿タイトル
        $slug = get_post_field('post_name', $code['id']); //スラッグ
        $excerpt = get_post_field('excerpt', $code['id']); //抜粋
        $content = get_post_field('post_content', $code['id']); //投稿内容
        $link = get_permalink($code['id']); //投稿url
        $edit_link = get_edit_post_link($code['id']); //編集url
        $meta = get_post_meta($code['id'], 'hogehoge', true); //カスタムフィールド

        $html = '<div class="hoge">';
        $html .= '<div class="hoge__content">'.$content.'</div>';
        $html .= '</div>';

        return $html;
      }
    }
  }

  //wpautopのために上手いこと処理するフィルター
  add_filter('the_content', 'run_my_shortcode', 8);
  function run_my_shortcode($content) {
    global $shortcode_tags;

    $orig_shortcode_tags = $shortcode_tags;
    remove_all_shortcodes();

    add_shortcode('my-embed-code', 'my_shortcode'); //ショートコード登録

    $content = do_shortcode($content); //ショートコード実行

    $shortcode_tags = $orig_shortcode_tags;

    return $content;
  }
}
