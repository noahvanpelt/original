<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Sydney
 */
get_header(); ?>

<div class="live-wrap">
  <div class="container">
    <h5>LIVE INFO</h5>

    <list-group>
      <?php $args = array(
          'numberposts' => 30,                //表示（取得）する記事の数
          'post_type' => 'liveinfo'    //投稿タイプの指定
        );
        $posts = get_posts( $args );
        if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
      <button type="button" data-toggle="collapse" data-target="#<?php the_id() ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></button>
      <div class="collapse mb-5" id="<?php the_id() ?>">
        <div class="card">
          <div class="card-body">
            <?php the_content() ?>
          </div>
        </div>
      </div>

      <?php endforeach; ?>
      <?php else : //記事が無い場合 ?>
      <li><p>記事はまだありません。</p></li>
      <?php endif;
      wp_reset_postdata(); //クエリのリセット ?>
    </ul>
  </div>
</div>

</div>
</div>








<?php get_footer(); ?>



<style>
@media only screen and (max-width: 724px){
  .site-description{
    font-size: 8px !important;
  }
}

button,
input[type="button"],
input[type="reset"],
input[type="submit"] {
background-color: transparent !important;
border: 1px solid #d65050;
    		color: #d65050;
	}

button:hover,
input[type="button"]:hover,
input[type="reset"]:hover,
input[type="submit"]:hover {
	background-color: #d65050 !important;
}

button:hover a{
  color:white !important;
  font-size: 16px;
}


.card, .card-body, .card p, .card-body p, .card a, .card-body a{
  word-break: break-all !important;
}
.card{
  width: 85% !important;
}


.live-wrap button{
  width: 85%;
  display: block;
  text-align: left;
  margin-top: 5px;
}


.live-wrap a{
  color: #d65050;
}

</style>
