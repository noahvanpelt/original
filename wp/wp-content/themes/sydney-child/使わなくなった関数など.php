

// archive-liveinfo.php カードでのアコーディオン

    <div id="accordion2">
      <?php $args = array(
          'numberposts' => 30,                //表示（取得）する記事の数
          'post_type' => 'liveinfo'    //投稿タイプの指定
        );
        $posts = get_posts( $args );
        if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
        <div class="card">
          <div class="card-header" >
            <h5>
              <button data-target="#<?php the_id() ?>" data-parent="#accordion2" data-toggle="collapse"><?php the_title(); ?></a>
            </h5>
          </div>
          <div id="<?php the_id() ?>" class="collapse">
            <div class="card-body">
              <?php the_content() ?>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
      <?php else : //記事が無い場合 ?>
      <li><p>記事はまだありません。</p></li>
      <?php endif;
      wp_reset_postdata(); //クエリのリセット ?>
    </div>
