<?php
/**　　投稿一覧ページはこちらを参照している。
* @package Sydney
*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<!--		<a href="<?php the_permalink(); ?>"> -->
		<a href="<?php the_permalink(); ?>">
		<div class="card">

			<?php if ( has_post_thumbnail() && ( get_theme_mod( 'index_feat_image' ) != 1 ) ) : ?>
<!--				<div class="entry-thumb">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('sydney-large-thumb'); ?></a>
				</div>   -->
			<?php endif; ?>
			<div class="card-body newslistcard">
				<div class="card-title">
<!--					<?php the_title( sprintf( '<button type="button"><h2 class="title-post entry-title"><a href="%s" rel="bookmark"> ', esc_url( get_permalink() ) ), '</a></h2></button>' ); ?>  -->
					<button type="button"><h2 class="title-post entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_post_thumbnail('sydney-large-thumb'); ?><?php the_title(); ?></a></h2></button>

					<?php if ( 'post' == get_post_type() && get_theme_mod('hide_meta_index') != 1 ) : ?>
						<div class="meta-post">
							<?php sydney_posted_on(); ?>
						</div><!-- .entry-meta -->
					<?php endif; ?>
				</div><!-- .entry-header -->

				<div class="entry-post">
					<?php if ( (get_theme_mod('full_content_home') == 1 && is_home() ) || (get_theme_mod('full_content_archives') == 1 && is_archive() ) ) : ?>
						<?php the_content(); ?>
					<?php else : ?>
						<?php the_excerpt(); ?>
					<?php endif; ?>

					<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'sydney' ),
						'after'  => '</div>',
					) );
					?>
				</div><!-- .entry-post -->

				<!--		</a>-->

				<div class="entry-footer">
					<?php sydney_entry_footer(); ?>
				</div><!-- .entry-footer -->
			</div>
		</div>
</a>
</article><!-- #post-## -->


<style>
.card{
	border: 0;
}

button{
	width: 100%;
	height: 100%;
}
button,
input[type="button"],
input[type="reset"],
input[type="submit"] {
background-color: transparent !important;
border: 1px solid #d65050;
    		color: #d65050;
	}

button:hover,
input[type="button"]:hover,
input[type="reset"]:hover,
input[type="submit"]:hover{
	background-color:  #d65050 !important;
	opacity: 0.8;
}

button:hover a{
	color: white !important;
}

button:active,
input[type="button"]:active,
input[type="reset"]:active,
input[type="submit"]:active {
	background-color: #d65050 !important;
	opacity: 0.5;
	color: white !important;
}


</style>
