<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Sydney
 */

get_header(); ?>

<div class="front-page">
  <div class="news-wrapper">
    <div class="container">
      <h5>NEWS</h5>
      <ul>
<?php
          $args = array(
            'numberposts' => 5,                //表示（取得）する記事の数
            'post_type' => ''    //投稿タイプの指定
          );
          $posts = get_posts( $args );
          if( $posts ) {
            foreach( $posts as $post ) {
              setup_postdata( $post );
?>
              <!-- =============================================================================- -->
              <!--                                                                                -->
              <!-- =============================================================================- -->
              <div class="parent_news_box">
                <a href="<?php the_permalink(); ?>">
                <div class="child_news_box">
                  <li><?php the_title(); ?></li>
                </div>
                </a>
              </div>
<?php
            }
?>


<?php
            foreach( $posts as $post ) {
              setup_postdata( $post );
?>
              <!-- =============================================================================- -->
              <!--                                                                                -->
              <!-- =============================================================================- -->
              <div class="parent_news_box">
                <a href="<?php the_permalink(); ?>">
                <div class="child_news_box2">
                  <li class="ttt">
                    <span class="top">
                      <span><?php the_title(); ?></span>
                    </span>
                    <span class="border">
                      <span><?php the_title(); ?></span>
                    </span>
                    <span class="border2">
                      <span><?php the_title(); ?></span>
                    </span>
                  </li>
                </div>
                </a>
              </div>

<?php
            }
          } else {
?>
          <li><p>記事はまだありません。</p></li>
<?php
          }
        wp_reset_postdata();
?>
      </ul>
    </div>
  </div>


<!-- =============================================================================- -->
<!--                                                                                -->
<!-- =============================================================================- -->
<style>
  .parent_news_box {
    position: relative;
  }
  .child_news_box {
    height:200px;
    border:1px solid black;
  }
  .child_news_box:hover:after {
      content: 'NEWS';
      width: 100%;
      height: 100%;
      background: RGB(240,128,128,0.8);
      position: absolute;
      top: 0;
      left: 0;
      text-align: center; /* 横中央 */
      line-height: 200px; /* 縦中央 */
  }
</style>






<!-- =============================================================================- -->
<!--                                                                                -->
<!-- =============================================================================- -->
<style>
.top {
  position: absolute;
  top: 0;
  left: 0px;
  z-index: 2;
  padding: 2px 0;
}
.border {
  position: absolute;
  top: 1px;
  left: 1px;
  z-index: 1;
}
.border2 {
  position: absolute;
  top: 1px;
  left: -1px;
  z-index: 1;
}
.top span{
  background: #f6f6f6;
  padding: 6px 0;
  word-wrap: break-word;
}
.border span{
  background: red;
  padding: 6px 0 8px;
  word-wrap: break-word;
}
.border2 span{
  background: red;
  padding: 6px 0 8px;
  word-wrap: break-word;
}
</style>



















  <div class="liveinfo-wrapper">
    <div class="container">
      <h5>LIVE INFO</h5>
      <ul>
        <?php $args = array(
            'numberposts' => 3,                //表示（取得）する記事の数
            'post_type' => 'liveinfo'    //投稿タイプの指定
          );
          $posts = get_posts( $args );
          if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
        <?php endforeach; ?>
        <?php else : //記事が無い場合 ?>
        <li><p>記事はまだありません。</p></li>
        <?php endif;
        wp_reset_postdata(); //クエリのリセット ?>
      </ul>
    </div>
  </div>

  <div class="sns-wrapper">
    <div class="container">
      <h5>SNS</h5>
    <?php echo do_shortcode('[instagram-feed]') ?>

    </div>
  </div>

  <div class="footmenu-wrapper">
    <div class="overlay">
      <div class="container">
        <h5></h5>
      </div>
    </div>
  </div>




</div>



<?php get_footer(); ?>
