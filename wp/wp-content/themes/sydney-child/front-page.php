<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Sydney
 * cutmized by Noah
 */

get_header(); ?>

<div class="front-page">


  <div class="news-wrapper">
    <div class="container">
      <h5>NEWS</h5>
      <ul>
        <?php $args = array(
            'numberposts' => 5,                //表示（取得）する記事の数
            'post_type' => ''    //投稿タイプの指定
          );
          $posts = get_posts( $args );
          if(  $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
          <?php endforeach; ?>
        <?php else : //記事が無い場合 ?>
          <li><p>記事はまだありません。</p></li>
        <?php endif;
        wp_reset_postdata(); //クエリのリセット ?>
      </ul>
    </div>
  </div>



  <div class="liveinfo-wrapper">
    <div class="container">
      <h5>LIVE INFO</h5>
      <ul>
        <?php $args = array(
            'numberposts' => 3,                //表示（取得）する記事の数
            'post_type' => 'liveinfo'    //投稿タイプの指定
          );
          $posts = get_posts( $args );
          if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
        <?php endforeach; ?>
        <?php else : //記事が無い場合 ?>
        <li><p>記事はまだありません。</p></li>
        <?php endif;
        wp_reset_postdata(); //クエリのリセット ?>
      </ul>
    </div>
  </div>

  <div class="sns-wrapper">
    <div class="container">
      <h5>Instagram</h5>
    <?php echo do_shortcode('[instagram-feed]') ?>

    </div>

  </div>
  <div class="releaselist-wrapper">
    <div class="container">
      <h5>RELEASE</h5>

      <!-- SLIDER WITH CONTROLS -->
      <div id="top-slider" class="carousel slide mb-5" data-ride="carousel" data-interval="50000">
        <div class="carousel-inner">
          <?php $args = array(
            'numberposts' => 15,            //表示（取得）する記事の数
            'post_type'   => 'release'     //投稿タイプの指定
          );                                         ?>
          <?php $posts = get_posts( $args );               ?>
          <?php if( $posts ):                              ?>
            <?php   $first_flg = True;                       ?>
            <?php   foreach( $posts as $post ):              ?>
              <?php     setup_postdata( $post );               ?>
              <?php     if ($first_flg) :                      ?>
                <div class="carousel-item active">
                  <?php       $first_flg = False                   ?>
                <?php     else:                                  ?>
                  <div class="carousel-item">
                  <?php     endif;                                 ?>
                  <div class="card-title">
                  <a href="<?php the_permalink() ?>">
                    <?php the_post_thumbnail('sydney-small-thumnb'); ?>
                  </a>
                </div>
                <div class="card-body">
                    <h3><?php the_title() ?></h3>
                  </div>
                </div>
              <?php   endforeach;                              ?>
            <?php endif;                                     ?>
            <?php wp_reset_postdata();                       ?> <!-- クエリのリセット -->

            <!-- CONTROLS -->
            <a href="#top-slider" class="carousel-control-prev" data-slide="prev">
              <span class="carousel-control-prev-icon"></span>
            </a>

            <a href="#top-slider" class="carousel-control-next" data-slide="next">
              <span class="carousel-control-next-icon"></span>
            </a>
          </div>
        </div>
      </div>

  </div>


  <div class="footmenu-wrapper">
    <div class="overlay-footmenu">
      <div class="container mainnav">
        <nav id="front-footnav" class="mainnav" role="navigation">
        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'fallback_cb' => 'sydney_menu_fallback' ) ); ?>
        </nav><!-- #site-navigation -->
      <h5></h5>
      </div>

      <div class="container sns-menu">
        <a href="https://facebook.com/bechoirmusic/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="https://twitter.com/be_choir/" <i class="fa fa-twitter" aria-hidden="true"></i></a>
        <a href="https://instagram.com/bechoir/"<i class="fa fa-instagram" aria-hidden="true"></i></a>
        <a href="https://www.youtube.com/bechoirmusic/"<i class="fa fa-youtube" aria-hidden="true"></i></a>
      </div>
    </div>
  </div>

</div>

<?php get_footer(); ?>


<style>


.releaselist-wrapper{
  background-color: whitesmoke;
}

.carousel-control-next span .carousel-control-prev span{
  margin:auto;
  color: whitesmoke;
}

#top-slider .carousel-inner{
  height: 100%;
  line-height: 100%;

}


#top-slider .carousel-item{
  height: 100%;
margin-top:0 ;
margin-bottom:0;


  text-align:center;
}

#top-slider h3{
  font-size: 16px;
  width: auto;
  padding-top:0;
  padding-bottom: 0;
  margin-top: 0;
  margin-bottom: 0;
}
#top-slider h5{
  font-size: 16px;
  padding:0;
  margin-top: 0;
  margin-bottom: 0;
}

#top-slider img{
  max-height: 200px;
  width: auto;
}


</style>
