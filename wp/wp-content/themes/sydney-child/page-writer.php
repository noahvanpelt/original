<?php
/*
Template Name:ライター一覧
*/
?>

<?php get_header() ?>

	<div id="primary" class="content-area col-md-9">
		<main id="main" class="post-wrap" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>
        <?php $users =get_users( array('orderby'=>ID,'order'=>ASC) );
        echo '<div class="writers masonry-layout posts-layout"><div class="container">';
        foreach($users as $user):
            $uid = $user->ID;
            $userData = get_userdata($uid);
            echo '<div class="card writer-profile hentry">';
                echo '<figure class="eyecatch">';
                    echo get_avatar( $uid ,300 );
                echo '</figure>';
                echo '<div class="card-body profiletxt">';
                    echo '<p class="name">'.$user->display_name.'</p>';
                    echo '<div class="description">'.$userData->user_description.'</div>';
                    echo '<div class="button"><a href="'.get_bloginfo(url).'/?author='.$uid.'">'.$user->display_name.'の記事一覧</a></div>';
                echo '</div>';
            echo '</div>';
        endforeach;
        echo '</div></div>'; ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>
			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->
<!--投稿者一覧を表示-->


<?php get_footer() ?>


<style>

.writers{
	padding: 0;
	webkit-transform: skewY(0deg);
	transform: skewY(0deg);
}


</style>
