<?php
/**
* The template used for displaying page content in page.php
*
* @package Sydney
*/
get_header(); ?>



<div class="release-wrap masonry-layout">
<div class="container">
  <h5>RELEASE</h5>
  <?php $args = array(
    'numberposts' => 30,                //表示（取得）する記事の数
    'post_type' => 'release'    //投稿タイプの指定
  );
  $posts = get_posts( $args );
  if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post );
  ?>

  <div class="card hentry">
    <div class="release-img">
      <a href="<?php the_permalink(); ?>">
      <?php
      if ( has_post_thumbnail() ) { // 投稿にアイキャッチ画像が割り当てられているかチェックします。
        the_post_thumbnail();
      } ?>
    </a>
    </div>

      <div class="card-title">
        <button type="button" data-toggle="collapse" data-target="#<?php the_id() ?>"><?php the_title(); ?></button>
      </div>
      <div class="collapse mb-5" id="<?php the_id() ?>">
      <div class="card-body">
        <?php the_content(); ?>
      </div>
    </div>
  </div>
  <?php endforeach; ?>
  <?php else : //記事が無い場合 ?>
    <p>記事はまだありません。</p>
  <?php endif;

  wp_reset_postdata(); //クエリのリセット ?>
</div>
</div>

</div>
</div>

<style>

.card{
  max-width: 28%;
  border-radius: 5px;
  border: solid 1px #000011;
  margin-bottom: 10px;
}

.card img{
  max-height: 100%;
  width: auto;
  border-radius: inherit;
}

.card-title{
  margin:10px;

}

.card-body{
  width: 90%;
  margin: auto;
}

</style>







<?php get_footer(); ?>



<style>

.card-title button{
  width: 100%;
}

.card-body{
  word-break: break-all !important;
}

.live-wrap button{
  width: 70%;
  display: block;
  text-align: left;
  margin-bottom: 5px;
}

</style>
