#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-07-28 15:45+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco - https://localise.biz/"

#: spotify-play-button-for-wordpress.php:132
msgid "Open in Spotify"
msgstr ""

#: spotify-play-button-for-wordpress.php:151
msgid "Spotify URI"
msgstr ""

#: spotify-play-button-for-wordpress.php:154
msgid "Insert the Spotify Play Button"
msgstr ""

#: spotify-play-button-for-wordpress.php:157
msgid ""
"Right-click on a playlist or album in Spotify, choose Share, click URI and "
"then paste the result in the box above. Finish by clicking the button."
msgstr ""

#: spotify-play-button-for-wordpress.php:161
msgid "Edit general settings for Spotify Play Button"
msgstr ""

#: spotify-play-button-for-wordpress.php:161
msgid ""
"or add parameters to your shortcode if you want to make this one special."
msgstr ""

#. Name of the plugin
msgid "Spotify Play Button for WordPress"
msgstr ""

#. Description of the plugin
msgid ""
"Show Spotify Play Button in any page or post with a simple hook. Example: "
"[spotifyplaybutton play=\"spotify:user:jonk:playlist:"
"65ujzBs6WTdWDIr17dOXUm\" view=\"list\" size=\"0\" sizetype=\"big\" "
"theme=\"black\"]. The best way to enter the value for the play-attribute is "
"to go to your playlist, right click and choose \"Copy Spotify URI\" and "
"paste it in the Text-view since you otherwise get a link and not the URI "
"pasted. The plugin also has a general settings page in wp-admin to manage "
"your default settings."
msgstr ""

#. URI of the plugin
msgid "https://wordpress.org/plugins/spotify-play-button-for-wordpress/"
msgstr ""

#. Author of the plugin
msgid "jonkastonka"
msgstr ""

#. Author URI of the plugin
msgid "http://jonk.pirateboy.net"
msgstr ""
