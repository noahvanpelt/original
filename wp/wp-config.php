<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'bechoircopy' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'root' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', 'root' );

/** MySQL のホスト名 */
define( 'DB_HOST', 'localhost' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8mb4' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=Dsj],[!N_#srqJ{z4B(ER(h,(~9sePr&p2),9U8yFu=(&)m2MhlXd]%G#[m5#yP' );
define( 'SECURE_AUTH_KEY',  '+%~[*$1(6@[xeYQ%$SBAt,UNE$48>vdvEAz^#pa|{]wzU^aD?Nx+NFpgmX*eAX9&' );
define( 'LOGGED_IN_KEY',    '7.t-7@Z1ou,ZGDF<}z>K0!E#8s:x[pZcT!c yHMT:(Qs+(C)&8n)!B,u%tQJ/s&2' );
define( 'NONCE_KEY',        'MY{-]?/=`Yq%6Lgv%9%Pp44_0YiNjdfPM|zLn:>^Wm$)L5MJDQ[i!K#gWqTkb,4c' );
define( 'AUTH_SALT',        'X$$7x.j,u.v#Hi<?N )TNJ?9R`#,d_(|sTH&W&p[tjh=Ul*:7;&i|c,HBis)3n)M' );
define( 'SECURE_AUTH_SALT', '?BN+8]d#~4F;Ki+4@CR:*q^ AF4(UFhg8Sbs54cp[Hey_?}N$[4*E-@y|R&qOYO%' );
define( 'LOGGED_IN_SALT',   'nMx<f V>NHhX 7xkMS4%IO74z5[(N8X^y%%gvV!19/15|Z_t7lPL@:W.MA@RD4l,' );
define( 'NONCE_SALT',       'QK:6s-$1yFkrx.V]{W9<QR WdiQKe7Ha7E#PLmKAfe~}:`(hgT2*?ybL<rF#A`^0' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
